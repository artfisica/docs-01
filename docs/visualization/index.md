# Get started with data visualisation (coming very soon!)

Two methods for online data visualisation are presented:

+ Histogram analyser: **web based tool for fast, cut-based analysis of data using online histograms**.

+ Analysis browser: **web based tool for the more advanced user, allowing the user to inspect the datasets more thoroughly**.

