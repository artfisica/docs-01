# Description of the 13 TeV ATLAS Open Data branches and variables

The **full list** of ROOT branches and variables contained within the 13 TeV ATLAS Open Data is presented below:

![](tab_04.png)

![](tab_05.png)
