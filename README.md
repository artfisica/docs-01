# Get Started

requirement: MkDocs

You can follow this instruction:
``` shell
git clone https://gitlab.cern.ch/lserkin/13tevatlasopendataguide2020.git
cd 13tevatlasopendataguide2020
mkdocs serve
```
To put it in App, you need to generate HTML by:
```
mkdocs build
```

Base coming from the work of our summer students
https://github.com/artfisica/ATLASOpenDataGuide

and previous 8 TeV release
https://github.com/artfisica/documentation-8tev-atlas-open-data

